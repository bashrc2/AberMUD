 /****************************************************************************\
 *                                                                            *
 *                    C R E A T O R    O F   L E G E N D S                    *
 *                             (AberMud Version 6)                            *
 *                                                                            *
 *  The Creator Of Legends System is (C) Copyright 1989 Alan Cox, All Rights  *
 *  Reserved.                                                                 *
 *                                                                            *
 *  This program is free software; you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation; either version 2 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program; if not, write to the Free Software               *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
 *                                                                            *
 \****************************************************************************/

struct IPC_Message
{
    long ms_Size;
    struct IPC_Message *ms_Next;
};

typedef struct IPC_Message MESSAGE;
#ifdef AMIGA_OLD
struct IPC_Port
{
    long po_SystemKey;
    MESSAGE *po_MessageList;
    long po_Flags;
    struct Task *po_Task;
    short po_Signal;
    short po_Open;
};

#else
struct IPC_Port
{
    int po_pid;
    int po_fd;
    int po_Open;
    int po_Flags;
    int po_SiloPtr;
    char po_Silo[8192];
};
#endif
typedef struct IPC_Port PORT;

#define PORT_SYSKEY 37612
#define FL_DELETE   (1<<4)
#define FL_TEMPORARY    (1<<4)
#define FL_FAULT    (1<<5)
#define NOWAIT      (1<<6)
#define FL_BOUND    (1<<7)
extern PORT *CreateMPort();
extern PORT *OpenMPort();
extern PORT *FindService();
