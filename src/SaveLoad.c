 /****************************************************************************\
 *                                                                            *
 *                    C R E A T O R    O F   L E G E N D S                    *
 *                             (AberMud Version 6)                            *
 *                                                                            *
 *  The Creator Of Legends System is (C) Copyright 1989 Alan Cox, All Rights  *
 *  Reserved.                                                                 *
 *                                                                            *
 *  This program is free software; you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation; either version 2 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program; if not, write to the Free Software               *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
 *                                                                            *
 \****************************************************************************/

#include "System.h"
#include <errno.h>

/*
 *  Disk Save/Load functions
 *
 *  1.00        AGC First Version
 *  1.01        AGC Saves substructures
 *  1.02        AGC Table saving code
 *  1.03-1.06   AGC Various tweaks, and substructure adds
 *  1.07        AGC Saves flag names
 *  1.08        AGC Runtime file format stuff
 *  1.09        AGC Hashed text lists
 *  1.10        AGC Register Optimisations
 *  1.11        AGC Setin Setout Sethere
 *  1.12        AGC UserTexts and frees texts right on Setin etc
 *  1.13        AGC Bug Fixes For 5.06 Release
 *  1.14        AGC Added Named table support
 *  1.15        AGC Added 5.08 Item table support
 *  1.16        AGC Fixed bug if you have single occurance of ""
 *  1.17        AGC Fixed Unix errno problem.
 *  1.18        AGC Fixed Unix errno problem properly
 *  1.19        AGC Taught it about USERFLAG2 and CONDEXIT
 *  1.20        AGC Tidied warnings out for 5.20
 *  1.21        AGC Loads superclass and daemontable fields.
 *  1.22        AGC Fixed SaveUniverse error that isnt bug
 *  1.23        AGC Saves BSX pictures
 *  1.25        AGC BSX save/load name bugfix
 */

Module  "DiskIO";
Version "1.24";
Author  "----*(A)";

extern int errno;
extern ITEM *ItemList;
/*
 *  Items are saved by direct ordered dump, all text dumped is done
 *  literally at present, not through the text list.
 *
 *  Item links are converted to itemnumber in the master list
 *  save for ->masternext which is unsaved as it is implied
 *
 *  For loading an initial value gives the number of items to load
 *  the items are allocated (but not substructs) into a linked list
 *  and an array of their pointers kept for conversions. Each item
 *  is loaded, then the whole array is freed
 */


/* Global Declarations */

static void SaveShort();
static unsigned short LoadShort();
static void SaveLong();
static unsigned long LoadLong();
static void SaveItem();
static ITEM *LoadItem();
static void SaveString();
static TPTR LoadString();
static void SaveBSX();
static BSXImage *LoadBSX();
static void SaveWord();
static int LoadWord();
static void SaveVocab();
static void LoadVocab();
static void WriteHeader();
static long ReadHeader();
static void SaveSub();
static void LoadSub();
static void LoadObject();
static void SaveObject();
static void SetTwo();
static long GetTwo();
static unsigned short *SaveAction();
static short *LoadAction();
static void LoadLine();
static void SaveLine();
static void LoadTable();
static void SaveTable();
static void LoadAllTables();
static void SaveAllTables();
static TABLE *LoadItemTable();
static void SaveItemTable();
static void RemoveEscapedQuotes();
static unsigned int ReadJsonString();
static unsigned int ReadJson();
static void SaveSubJson();
static void SaveObjectJson();
static void SavePlayerJson();
static void SaveRoomJson();
static void SaveItemJson();
static int SaveSystemRoomsJson();
static int SaveSystemItemsJson();
static void SaveTableJson();
static void SaveLineJson();
static unsigned short *SaveActionJson();
static unsigned long indirect_room_ID();
static unsigned int RoomExits();
static int is_room();
static int is_player();
char *TextOfWithoutQuotes();

static ITEM **ItemArray;    /* Used for item loaders */
static long Load_Format=-1;
static int Load_Error=0;

unsigned short *SaveActionJson(file,c,index,extra)
FILE *file;
register unsigned short *c;
unsigned int index;
char *extra;
{
    int cc=*c;
    TPTR t;
    ITEM *i;
    register char *ptr;
    if(index>0)
        fprintf(file, "%s\n", ",");
    fprintf(file, "%s\t\t\t\t\t\t\"Act\": %s ", extra, "[");
    fprintf(file, "%hu", *c++);
    ptr=Cnd_Table[cc];
    while(*ptr!=' ')
    {
        switch(*ptr++)
        {
        case '$':;
        case 'T':t=(TPTR )GetTwo(c);
             c+=2;
             if(t==(TPTR )1)
                 fprintf(file, ",%d", 0);
             else
             {
                if(t==(TPTR)3)
                    fprintf(file, ",%d", 3);
                else
                {
                    fprintf(file, ",%d", 1);
                    fprintf(file, ",\"%s\"", TextOfWithoutQuotes(t));
                }
             }
             break;
        case 'I':i=(ITEM *)GetTwo(c);
             c+=2;
             if(i==(ITEM *)1)
             {
                fprintf(file, ",%d", 1);
                break;
             }
             if(i==(ITEM *)3)
             {
                fprintf(file, ",%d", 3);
                break;
             }
             if(i==(ITEM *)5)
             {
                fprintf(file, ",%d", 5);
                break;
             }
             if(i==(ITEM *)7)
             {
                fprintf(file, ",%d", 7);
                break;
             }
             if(i==(ITEM *)9)
             {
                fprintf(file, ",%d", 9);
                break;
             }
             fprintf(file, ",%d", 0);
             if(i==NULL)
                 fprintf(file, ",%lu", -1L);
             else
                 fprintf(file, ",%lu", MasterNumber(i));
             break;
        case 'C':;
        case 'R':;
        case 'O':;
        case 'P':;
        case 'c':;
        case 'B':;
        case 'F':;
        case 'v':;
        case 'p':;
        case 'n':;
        case 'a':;
        case 't':;
        case 'N':fprintf(file, ",%hu", *c++);
             break;
        default:Error("Save: Bad Cnd_Table Entry");
        }
    }
    fprintf(file, " %s", "]");
    return(c);
}

void SaveLineJson(file,l,line_index,extra)
FILE *file;
LINE *l;
unsigned int line_index;
char *extra;
{
    unsigned short *a=l->li_Data;
    unsigned int index=0;
    if(line_index>0)
        fprintf(file,"%s\n", ",");
    fprintf(file,"%s\t\t\t\t%s\n", extra, "{");
    fprintf(file,"%s\t\t\t\t\t\"Verb\": %hd,\n", extra, l->li_Verb);
    fprintf(file,"%s\t\t\t\t\t\"Noun 1\": %hd,\n", extra, l->li_Noun1);
    fprintf(file,"%s\t\t\t\t\t\"Noun 2\": %hd,\n", extra, l->li_Noun2);
    fprintf(file,"%s\t\t\t\t\t\"Actions\": %s\n", extra, "[");
    while(*a!=CMD_EOL)
    {
        a=SaveActionJson(file,a,index++,extra);
    }
    fprintf(file,"\n%s\t\t\t\t\t%s\n", extra, "]");
    fprintf(file,"%s\t\t\t\t%s", extra, "}");
}

static void RemoveEscapedQuotes(str)
char *str;
{
    int pos=1;
    unsigned int i;

    while(pos > 0) {
        pos = -1;
        for (i = 0; i < strlen(str)-1; i++) {
            if (str[i] == (char)92) {
                if (str[i+1] == '"') {
                    pos = i;
                }
            }
        }
        if(pos == -1)
            continue;

        for (i = pos; i < strlen(str)-1; i++) {
            str[pos] = str[pos+1];
        }
        str[strlen(str)-1]=0;
    }
}

static unsigned int ReadJsonString(str, depth, depthindex, variable, value)
char *str;
unsigned int depth;
unsigned int *depthindex;
char *variable;
char *value;
{
    int quote_pos=-1,value_pos=-1;
    unsigned int i,j,k,search_start_pos,quote_ctr=0;

    variable[0]=0;
    value[0]=0;

    for (i = 0; i < strlen(str); i++) {
        if ((str[i] == '{') || (str[i] == '[') ||
            (str[i] == ',') || (str[i] == ':')) {
            quote_pos=-1;
            quote_ctr=0;
        }

        /* increasing depth */
        if ((str[i] == '{') || (str[i] == '[')) {
            depthindex[depth]=0;
            if (depth<16)
                depth++;
        }

        /* decreasing depth */
        if ((str[i] == '}') || (str[i] == ']'))
            depth--;

        if (str[i] == ',') {
            if (depth>0) {
                depthindex[depth-1]++;
            }

            if (value_pos>-1) {
                for(j=(unsigned int)value_pos; j < i; j++) {
                    value[j-(unsigned int)value_pos] = str[j];
                }
                value[j-(unsigned int)value_pos]=0;
                RemoveEscapedQuotes(value);
                value_pos=-1;
            }
        }

        /* get the position where the value starts */
        if (str[i] == ':') {
            /* starting position for the value */
            if (str[i+1] == ' ')
                value_pos=(int)i+2;
            else
                value_pos=(int)i+1;
        }

        /* extract values between square brackets */
        if ((value_pos>-1) && (str[i] == '[')) {
            search_start_pos=i+1;
            if(str[search_start_pos]==' ')
                search_start_pos++;
            for(j=strlen(str)-1;j>search_start_pos;j--) {
                if (str[j] == ']') {
                    if (str[j-1]==' ')
                        j--;
                    break;
                }
            }
            if(j > search_start_pos) {
                for(k=search_start_pos; k < j; k++) {
                    value[k-search_start_pos] = str[k];
                }
                value[k-search_start_pos]=0;
                value_pos=-1;
                i=j;
            }
        }

        /* extract strings */
        if (str[i] == '"') {
            quote_ctr++;

            if(quote_ctr == 2) {
                if(quote_pos>-1) {
                    if(value_pos==-1) {
                        for(j=(unsigned int)quote_pos; j < i; j++) {
                            variable[j-(unsigned int)quote_pos] = str[j];
                        }
                        variable[j-(unsigned int)quote_pos]=0;
                    }
                    else {
                        for(j=(unsigned int)quote_pos; j < i; j++) {
                            value[j-(unsigned int)quote_pos] = str[j];
                        }
                        value[j-(unsigned int)quote_pos]=0;
                        RemoveEscapedQuotes(value);
                        value_pos=-1;
                    }
                    quote_pos=-1;
                }
            }
            else {
                quote_pos=(int)i+1;
            }
        }
    }

    /* Handle unterminated values */
    if (value_pos>-1) {
        for(j=(unsigned int)value_pos; j < i; j++) {
            if((str[j]==',') || (str[j]=='\n'))
                break;
            value[j-(unsigned int)value_pos] = str[j];
        }
        value[j-(unsigned int)value_pos]=0;
        RemoveEscapedQuotes(value);
    }

    return depth;
}

static unsigned int ReadJson(file, line, depth, depthindex, variable, value)
FILE *file;
char *line;
unsigned int depth;
unsigned int *depthindex;
char *variable;
char *value;
{
    if(fgets(line, sizeof(line), file) == NULL) {
        variable[0]=0;
        value[0]=0;
        return 0;
    }

    return ReadJsonString(line, depth, depthindex, variable, value);
}

void SaveTableJson(file,t,index,extra_indents)
FILE *file;
TABLE *t;
unsigned int index;
unsigned char extra_indents;
{
    char extra[4];
    register LINE *l=t->tb_First;
    unsigned int i,line_index=0;

    if(l==NULL)
        return;

    extra[0]=0;
    if(extra_indents>0) {
        for (i=0;i<(unsigned int)extra_indents;i++)
            extra[i]='\t';
        extra[i]=0;
    }

    if (index>0)
        fprintf(file, "%s\n", ",");
    fprintf(file, "%s\t\t%s\n", extra, "{");
    fprintf(file, "%s\t\t\t\"TableNumber\": %hd,\n", extra, t->tb_Number);
    fprintf(file, "%s\t\t\t\"TableName\": \"%s\",\n", extra, TextOfWithoutQuotes(t->tb_Name));
    fprintf(file, "%s\t\t\t\"TableLines\": %s\n", extra, "[");
    while(l)
    {
        SaveLineJson(file,l,line_index++,extra);
        l=l->li_Next;
    }
    fprintf(file, "\n%s\t\t\t%s\n", extra, "]");
    fprintf(file, "%s\t\t%s", extra, "}");
}

static void SaveSubJson(file,s,index)
FILE *file;
register SUB *s;
unsigned int index;
{
    unsigned int i;
    if (index > 0) {
        fprintf(file, "%s\n", ",");
    }

    switch(s->pr_Key)
    {
        case KEY_INOUTHERE:
            fprintf(file, "\t\t\t\t{\n\t\t\t\t\t\"Subtype\": \"%s\",\n", "in out there");
            fprintf(file, "\t\t\t\t\t\"In\": \"%s\",\n", TextOfWithoutQuotes(((INOUTHERE *)s)->io_InMsg));
            fprintf(file, "\t\t\t\t\t\"Out\": \"%s\",\n", TextOfWithoutQuotes(((INOUTHERE *)s)->io_OutMsg));
            fprintf(file, "\t\t\t\t\t\"Here\": \"%s\"\n\t\t\t\t}", TextOfWithoutQuotes(((INOUTHERE *)s)->io_HereMsg));
            break;
        case KEY_USERFLAG2:
            fprintf(file, "\t\t\t\t{\n\t\t\t\t\t\"Subtype\": \"%s\",\n", "user flag 2");
        case KEY_USERFLAG:
            if(s->pr_Key!=KEY_USERFLAG2)
                fprintf(file, "\t\t\t\t{\n\t\t\t\t\t\"Subtype\": \"%s\",\n", "user flag");
            fprintf(file, "\t\t\t\t\t\"Flags\": %s", "[");
            for(i = 0; i < 8; i++) {
                if (i > 0)
                    fprintf(file, "%s", ",");
                fprintf(file, "%hu", ((USERFLAG *)s)->uf_Flags[i]);
            }
            for(i = 0; i < 8; i++) {
                if (((USERFLAG *)s)->uf_Items[i])
                    fprintf(file, ",%lu", ((USERFLAG *)s)->uf_Items[i]->it_ID);
                else
                    fprintf(file, ",%d", 0);
            }
            fprintf(file, "%s", "]\n\t\t\t\t}");
            break;
        case KEY_CONTAINER:
            fprintf(file, "\t\t\t\t{\n\t\t\t\t\t\"Subtype\": \"%s\",\n", "container");
            fprintf(file, "\t\t\t\t\t\"Volume\": %hu,\n", ((CONTAINER *)s)->co_Volume);
            fprintf(file, "\t\t\t\t\t\"Flags\": %hu\n\t\t\t\t}", ((CONTAINER *)s)->co_Flags);
            break;
        case KEY_CHAIN:
            if(((CHAIN *)s)->ch_Chained) {
                fprintf(file, "\t\t\t\t{\n\t\t\t\t\t\"Subtype\": \"%s\",\n", "chain");
                fprintf(file, "\t\t\t\t\t\"ID\": %lu\n\t\t\t\t}", ((CHAIN *)s)->ch_Chained->it_ID);
            }
            break;
        case KEY_ROOM:
            fprintf(file, "\t\t\t\t{\n\t\t\t\t\t\"Subtype\": \"%s\",\n", "room");
            fprintf(file, "\t\t\t\t\t\"Short\": \"%s\",\n", TextOfWithoutQuotes(((ROOM *)s)->rm_Short));
            fprintf(file, "\t\t\t\t\t\"Long\": \"%s\",\n", TextOfWithoutQuotes(((ROOM *)s)->rm_Long));
            fprintf(file, "\t\t\t\t\t\"Flags\": %hu\n\t\t\t\t}", ((ROOM *)s)->rm_Flags);
            break;
        case KEY_OBJECT:
            fprintf(file, "\t\t\t\t{\n\t\t\t\t\t\"Subtype\": \"%s\",\n", "object");
            fprintf(file, "\t\t\t\t\t\"Text\": %s", "[");
            for (i = 0; i < 4; i++) {
                if (i > 0)
                    fprintf(file, "%s", ",");
                fprintf(file, "\"%s\"", TextOfWithoutQuotes(((OBJECT *)s)->ob_Text[i]));
            }
            fprintf(file, "%s,\n", "]");
            fprintf(file, "\t\t\t\t\t\"Size\": %hu,\n", ((OBJECT *)s)->ob_Size);
            fprintf(file, "\t\t\t\t\t\"Weight\": %hu,\n", ((OBJECT *)s)->ob_Weight);
            fprintf(file, "\t\t\t\t\t\"Flags\": %hu\n\t\t\t\t}", ((OBJECT *)s)->ob_Flags);
            break;
        case KEY_PLAYER:
            fprintf(file, "\t\t\t\t{\n\t\t\t\t\t\"Subtype\": \"%s\",\n", "player");
            fprintf(file, "\t\t\t\t\t\"UserKey\": %hu,\n", ((PLAYER *)s)->pl_UserKey);
            fprintf(file, "\t\t\t\t\t\"Size\": %hu,\n", ((PLAYER *)s)->pl_Size);
            fprintf(file, "\t\t\t\t\t\"Weight\": %hu,\n", ((PLAYER *)s)->pl_Weight);
            fprintf(file, "\t\t\t\t\t\"Strength\": %hu,\n", ((PLAYER *)s)->pl_Strength);
            fprintf(file, "\t\t\t\t\t\"Flags\": %hu,\n", ((PLAYER *)s)->pl_Flags);
            fprintf(file, "\t\t\t\t\t\"Level\": %hu,\n", ((PLAYER *)s)->pl_Level);
            fprintf(file, "\t\t\t\t\t\"Score\": %lu\n\t\t\t\t}", ((PLAYER *)s)->pl_Score);
            break;
        case KEY_GENEXIT:
            fprintf(file, "\t\t\t\t{\n\t\t\t\t\t\"Subtype\": \"%s\",\n", "genexit");
            fprintf(file, "\t\t\t\t\t\"RoomID\": %s", "[");
            for(i = 0; i < 12; i++) {
                if(((GENEXIT *)s)->ge_Dest[i]) {
                    if(i>0)
                        fprintf(file, "%s", ",");
                    fprintf(file, "%lu", ((GENEXIT *)s)->ge_Dest[i]->it_ID);
                }
                else {
                    if(i>0)
                        fprintf(file, "%s", ",0");
                    else
                        fprintf(file, "%s", "0");
                }
            }
            fprintf(file, "%s\n\t\t\t\t}", "]");
            break;
        case KEY_CONDEXIT:
            fprintf(file, "\t\t\t\t{\n\t\t\t\t\t\"Subtype\": \"%s\",\n", "condexit");
            if(((CONDEXIT *)s)->ce_Dest)
                fprintf(file, "\t\t\t\t\t\"ID\": %lu,\n", ((CONDEXIT *)s)->ce_Dest->it_ID);
            fprintf(file, "\t\t\t\t\t\"Table\": %hu,\n", (short)((CONDEXIT *)s)->ce_Table);
            fprintf(file, "\t\t\t\t\t\"ExitNumber\": %hu\n\t\t\t\t}", (short)((CONDEXIT *)s)->ce_ExitNumber);
            break;
        case KEY_MSGEXIT:
            fprintf(file, "\t\t\t\t{\n\t\t\t\t\"Subtype\": \"%s\",\n", "msgexit");
            if(((MSGEXIT *)s)->me_Dest)
                fprintf(file, "\t\t\t\t\t\"Dest\": %lu,\n", ((MSGEXIT *)s)->me_Dest->it_ID);
            fprintf(file, "\t\t\t\t\t\"Text\": \"%s\",\n", TextOfWithoutQuotes(((MSGEXIT *)s)->me_Text));
            fprintf(file, "\t\t\t\t\t\"ExitNumber\": %hu\n\t\t\t\t}", ((MSGEXIT *)s)->me_ExitNumber);
            break;
        case KEY_INHERIT:
            if(((INHERIT *)s)->in_Master) {
                fprintf(file, "\t\t\t\t{\n\t\t\t\t\t\"Subtype\": \"%s\",\n", "inherit");
                fprintf(file, "\t\t\t\t\t\"Master\": %lu\n\t\t\t\t}", ((INHERIT *)s)->in_Master->it_ID);
            }
            break;
        case KEY_DUPED:
            if(((DUP *)s)->du_Master) {
                fprintf(file, "\t\t\t\t{\n\t\t\t\t\"Subtype\": \"%s\",\n", "duped");
                fprintf(file, "\t\t\t\t\t\"Master\": %lu\n\t\t\t\t}", ((DUP *)s)->du_Master->it_ID);
            }
            break;
        case KEY_USERTEXT:
            fprintf(file, "\t\t\t\t{\n\t\t\t\t\t\"Subtype\": \"%s\",\n", "usertext");
            fprintf(file, "\t\t\t\t\t\"Text\": %s", "[");
            for(i = 0; i < 8; i++) {
                if (i>0)
                    fprintf(file, "%s", ",");
                fprintf(file, "\"%s\"", TextOfWithoutQuotes(((USERTEXT *)s)->ut_Text[i]));
            }
            fprintf(file, "%s", "]\n\t\t\t\t}");
            break;
        default:Error("Unknown Subtype");
    }
}

static int SaveRoomSubJson(file,s,index,ID,desc,exits)
FILE *file;
register SUB *s;
unsigned int index;
unsigned long ID;
unsigned char desc;
unsigned char exits;
{
    unsigned int i, first=1;
    unsigned long exitRoomID=0;

    if (s->pr_Key == KEY_ROOM) {
        if ((desc==1) && (exits==0))
            fprintf(file, "    \"$rid=%lu$\": {\n", ID);

        fprintf(file, "        \"name\": \"%s\",\n", TextOfWithoutQuotes(((ROOM *)s)->rm_Short));
        fprintf(file, "        \"description\": \"%s\",\n", TextOfWithoutQuotes(((ROOM *)s)->rm_Long));
        fprintf(file, "        \"eventOnEnter\": \"%s\",\n", "");

        if (!((desc==1) && (exits==0))) {
            fprintf(file, "        \"eventOnLeave\": \"%s\"\n", "");
            fprintf(file, "    %s,\n", "}");
        }
        else {
            fprintf(file, "        \"eventOnLeave\": \"%s\"", "");
        }
        return 0;
    }
    else {
        char* exitDir[] = {"north","east","south","west","up","down","in","out","A","B","in","out"};

        for (i=0;i<12;i++) {
            if (((GENEXIT *)s)->ge_Dest[i]) {

                exitRoomID=0;
                if (is_room(((GENEXIT *)s)->ge_Dest[i])!=0)
                    exitRoomID=((GENEXIT *)s)->ge_Dest[i]->it_ID;
                else
                    exitRoomID=indirect_room_ID(((GENEXIT *)s)->ge_Dest[i]);

                if (exitRoomID > 0) {
                    if (first!=0) {
                        if ((desc==0) && (exits==1))
                            fprintf(file, "    \"$rid=%lu$\": {\n", ID);
                        else
                            fprintf(file, "%s\n", ",");
                        fprintf(file, "        \"exits\": {%s\n", "");

                        fprintf(file, "            \"%s\": \"$rid=%lu$\"", exitDir[i], exitRoomID);
                    }
                    else
                        fprintf(file, ",\n            \"%s\": \"$rid=%lu$\"", exitDir[i], exitRoomID);
                    first=0;
                }
            }
        }

        if (first==0) {
            if (!((desc==0) && (exits==1))) {
                fprintf(file, "\n        %s\n", "}");
                fprintf(file, "    %s,\n", "}");
            }
            else {
                fprintf(file, "\n        %s,\n", "}");
            }
            return 0;
        }
        return 1;
    }
}

static unsigned int RoomExits(i)
register ITEM *i;
{
    SUB *n;
    unsigned long exitRoomID=0;
    unsigned int j,no_of_exits=0;

    if(is_room(i)==0)
        return no_of_exits;

    KillEventQueue(i);
    n=i->it_Properties;
    while(n)
    {
        if (n->pr_Key == KEY_GENEXIT) {
            for (j=0;j<12;j++) {
                if (((GENEXIT *)n)->ge_Dest[j]) {

                    if (is_room(((GENEXIT *)n)->ge_Dest[j])!=0)
                        exitRoomID=((GENEXIT *)n)->ge_Dest[j]->it_ID;
                    else
                        exitRoomID=indirect_room_ID(((GENEXIT *)n)->ge_Dest[j]);

                    if (exitRoomID > 0)
                        no_of_exits++;
                }
            }
        }
        n=n->pr_Next;
    }
    return no_of_exits;
}

static int SavePlayerSubJson(file,s,index,ID,playerstats,inoutthere,name,eventfile,roomID)
FILE *file;
register SUB *s;
unsigned int index;
unsigned long ID;
unsigned char playerstats;
unsigned char inoutthere;
char *name;
FILE *eventfile;
unsigned long roomID;
{
    if (s->pr_Key == KEY_PLAYER) {
        if ((playerstats==1) && (inoutthere==0))
            fprintf(file, "\n    \"%lu\": {\n", ID);
        else
            fprintf(file, "%s\n", "");

        /* id;room;isAttackable;isStealable;isKillable;isAggressive;corpseTTL;respawn */
        if (roomID>0)
            fprintf(eventfile, "0|spawnNPC|%lu;$rid=%lu$;1;0;1;1;30;10\n", ID, roomID);

        fprintf(file, "        \"name\": \"%s\",\n", name);
        fprintf(file, "        \"inv\" : [%s],\n","");
        fprintf(file, "        \"path\" : [%s],\n","");
        fprintf(file, "        \"moveDelay\" : \"%d\",\n", 300);
        fprintf(file, "        \"moveType\" : \"%s\",\n", "");
        fprintf(file, "        \"vocabulary\" : \"%s\",\n","");
        fprintf(file, "        \"talkDelay\" : \"%d\",\n", 300);
        fprintf(file, "        \"randomFactor\" : \"%d\",\n", 100);
        fprintf(file, "        \"hp\" : \"%d\",\n",24332);
        fprintf(file, "        \"charge\" : \"%d\",\n",1233);
        fprintf(file, "        \"lvl\" : \"%hu\",\n", ((PLAYER *)s)->pl_Level);
        fprintf(file, "        \"exp\" : \"%d\",\n",32);
        fprintf(file, "        \"str\" : \"%hu\",\n", ((PLAYER *)s)->pl_Strength);
        fprintf(file, "        \"siz\" : \"%hu\",\n", ((PLAYER *)s)->pl_Size);
        fprintf(file, "        \"wei\" : \"%hu\",\n", ((PLAYER *)s)->pl_Weight);
        fprintf(file, "        \"per\" : \"%d\",\n",3);
        fprintf(file, "        \"endu\" : \"%d\",\n",1);
        fprintf(file, "        \"cha\" : \"%d\",\n",4);
        fprintf(file, "        \"inte\" : \"%d\",\n",2);
        fprintf(file, "        \"agi\" : \"%d\",\n",6);
        fprintf(file, "        \"luc\" : \"%d\",\n",1);
        fprintf(file, "        \"cred\" : \"%d\",\n",122);
        fprintf(file, "        \"clo_head\" : \"%d\",\n",0);
        fprintf(file, "        \"clo_larm\" : \"%d\",\n",0);
        fprintf(file, "        \"clo_rarm\" : \"%d\",\n",0);
        fprintf(file, "        \"clo_lhand\" : \"%d\",\n",0);
        fprintf(file, "        \"clo_rhand\" : \"%d\",\n",0);
        fprintf(file, "        \"clo_chest\" : \"%d\",\n",0);
        fprintf(file, "        \"clo_lleg\" : \"%d\",\n",0);
        fprintf(file, "        \"clo_rleg\" : \"%d\",\n",0);
        fprintf(file, "        \"clo_feet\" : \"%d\",\n",0);
        fprintf(file, "        \"imp_head\" : \"%d\",\n",0);
        fprintf(file, "        \"imp_larm\" : \"%d\",\n",0);
        fprintf(file, "        \"imp_rarm\" : \"%d\",\n",0);
        fprintf(file, "        \"imp_lhand\" : \"%d\",\n",0);
        fprintf(file, "        \"imp_rhand\" : \"%d\",\n",0);
        fprintf(file, "        \"imp_chest\" : \"%d\",\n",0);
        fprintf(file, "        \"imp_lleg\" : \"%d\",\n",0);
        fprintf(file, "        \"imp_rleg\" : \"%d\",\n",0);

        if (!((playerstats==1) && (inoutthere==0))) {
            fprintf(file, "        \"imp_feet\" : \"%d\"\n", 0);
            fprintf(file, "    %s,\n", "}");
        }
        else {
            fprintf(file, "        \"imp_feet\" : \"%d\"", 0);
        }
    }
    else {
        if ((playerstats==0) && (inoutthere==1))
            fprintf(file, "    \"%lu\": {\n", ID);
        else
            fprintf(file, "%s\n", ",");

        if (strlen((char*)((INOUTHERE *)s)->io_InMsg)>1)
            fprintf(file, "        \"inDescription\": \"%s\",\n", TextOfWithoutQuotes(((INOUTHERE *)s)->io_InMsg));
        else
            fprintf(file, "        \"inDescription\": \"%s\",\n", "arrives");

        if (strlen((char*)((INOUTHERE *)s)->io_OutMsg)>1)
            fprintf(file, "        \"outDescription\": \"%s\",\n", TextOfWithoutQuotes(((INOUTHERE *)s)->io_OutMsg));
        else
            fprintf(file, "        \"outDescription\": \"%s\",\n", "goes");

        if (!((playerstats==0) && (inoutthere==1))) {
            fprintf(file, "        \"lookDescription\": \"%s\"\n", TextOfWithoutQuotes(((INOUTHERE *)s)->io_HereMsg));
            fprintf(file, "    %s,", "}");
        }
        else {
            fprintf(file, "        \"lookDescription\": \"%s\",", TextOfWithoutQuotes(((INOUTHERE *)s)->io_HereMsg));
        }
    }
    return 0;
}

static int SaveItemSubJson(file,s,index,ID,name,eventfile,roomID)
FILE *file;
register SUB *s;
unsigned int index;
unsigned long ID;
char *name;
FILE *eventfile;
unsigned long roomID;
{
    unsigned int i,shortest_index=0,longest_index=0;
    char *shorter_name=name;

    if (roomID>0)
        fprintf(eventfile, "0|spawnItem|%lu;$rid=%lu$;0;0\n", ID, roomID);

    fprintf(file, "    \"%lu\": {\n", ID);
    if (strlen(name)>4) {
        if ((name[0]=='T') || (name[0]=='t')) {
            if ((name[1]=='h') && (name[2]=='e') && (name[3]==' '))
                shorter_name=&name[4];
        }
    }
    fprintf(file, "        \"name\": \"%s\",\n", shorter_name);
    for(i=1;i<4;i++) {
        if (strlen((char*)((OBJECT *)s)->ob_Text[i])<2)
            continue;

        if (strlen((char*)((OBJECT *)s)->ob_Text[i]) > strlen((char*)((OBJECT *)s)->ob_Text[longest_index]))
            longest_index=i;
        if ((strlen((char*)((OBJECT *)s)->ob_Text[i]) < strlen((char*)((OBJECT *)s)->ob_Text[shortest_index])) || (strlen((char*)((OBJECT *)s)->ob_Text[shortest_index])<2))
            shortest_index=i;
    }
    fprintf(file, "        \"short_description\": \"%s\",\n", TextOfWithoutQuotes(((OBJECT *)s)->ob_Text[shortest_index]));
    fprintf(file, "        \"long_description\": \"%s\",\n", TextOfWithoutQuotes(((OBJECT *)s)->ob_Text[longest_index]));
    fprintf(file, "        \"clo_head\": \"%d\",\n",0);
    fprintf(file, "        \"clo_larm\": \"%d\",\n",0);
    fprintf(file, "        \"clo_rarm\": \"%d\",\n",0);
    fprintf(file, "        \"clo_lhand\": \"%d\",\n",0);
    fprintf(file, "        \"clo_rhand\": \"%d\",\n",0);
    fprintf(file, "        \"clo_chest\": \"%d\",\n",0);
    fprintf(file, "        \"clo_lleg\": \"%d\",\n",0);
    fprintf(file, "        \"clo_rleg\": \"%d\",\n",0);
    fprintf(file, "        \"clo_feet\": \"%d\",\n",0);
    fprintf(file, "        \"imp_head\": \"%d\",\n",0);
    fprintf(file, "        \"imp_larm\": \"%d\",\n",0);
    fprintf(file, "        \"imp_rarm\": \"%d\",\n",0);
    fprintf(file, "        \"imp_lhand\": \"%d\",\n",0);
    fprintf(file, "        \"imp_rhand\": \"%d\",\n",0);
    fprintf(file, "        \"imp_chest\": \"%d\",\n",0);
    fprintf(file, "        \"imp_lleg\": \"%d\",\n",0);
    fprintf(file, "        \"imp_rleg\": \"%d\",\n",0);
    fprintf(file, "        \"imp_feet\": \"%d\",\n",0);
    fprintf(file, "        \"mod_str\": \"%d\",\n",0);
    fprintf(file, "        \"mod_per\": \"%d\",\n",0);
    fprintf(file, "        \"mod_endu\": \"%d\",\n",0);
    fprintf(file, "        \"mod_cha\": \"%d\",\n",0);
    fprintf(file, "        \"mod_inte\": \"%d\",\n",0);
    fprintf(file, "        \"mod_agi\": \"%d\",\n",0);
    fprintf(file, "        \"mod_luc\": \"%d\",\n",0);
    fprintf(file, "        \"weight\": \"%hu\",\n",((OBJECT *)s)->ob_Weight);
    fprintf(file, "        \"article\": \"%s\"\n","a");
    fprintf(file, "    %s,\n", "}");

    return 0;
}

static void SaveObjectJson(file,i,ID)
register FILE *file;
register ITEM *i;
unsigned long ID;
{
    SUB *n;
    unsigned int index;
    KillEventQueue(i);
    if (ID==1) {
        fprintf(file, "\t%s\n", "\"Items\": [");
    }
    else {
        fprintf(file, "%s\n", ",");
    }
    fprintf(file, "\t\t%s\n", "{");
    fprintf(file, "\t\t\t\"ItemID\": %lu,\n", ID);
    if(i->it_Name)
        fprintf(file, "\t\t\t\"ItemName\": \"%s\",\n", TextOfWithoutQuotes(i->it_Name));
    fprintf(file, "\t\t\t\"ItemNoun\": %hd,\n", i->it_Noun);
    fprintf(file, "\t\t\t\"ItemAdjective\": %hd,\n", i->it_Adjective);
    fprintf(file, "\t\t\t\"ItemActorTable\": %hd,\n", i->it_ActorTable);
    fprintf(file, "\t\t\t\"ItemActionTable\": %hd,\n", i->it_ActionTable);
    fprintf(file, "\t\t\t\"ItemUsers\": %hd,\n", i->it_Users);
    fprintf(file, "\t\t\t\"ItemState\": %hd,\n", i->it_State);
    fprintf(file, "\t\t\t\"ItemClass\": %hd,\n", i->it_Class);
    fprintf(file, "\t\t\t\"ItemPerception\": %hd,\n", i->it_Perception);
    fprintf(file, "\t\t\t\"ItemZone\": %hd,\n", i->it_Zone);
    if(i->it_Next)
        fprintf(file, "\t\t\t\"ItemNext\": %lu,\n", i->it_Next->it_ID);
    if(i->it_Children)
        fprintf(file, "\t\t\t\"ItemChildren\": %lu,\n", i->it_Children->it_ID);
    if(i->it_Parent)
        fprintf(file, "\t\t\t\"ItemParent\": %lu,\n", i->it_Parent->it_ID);
    n=i->it_Properties;
    if(i->it_Superclass)
    {
        fprintf(file, "\t\t\t\"ItemSuperclassID\": %lu", i->it_Superclass->it_ID);
    }
    else {
        fprintf(file, "\t\t\t\"ItemSuperclassID\": %d", 0);
    }
    if(n==0) {
        fprintf(file, "\n\t\t%s", "}");
        return;
    }
    else {
        fprintf(file, "%s\n", ",");
    }
    fprintf(file, "\t\t\t\"Properties\": %s\n", "[");
    index = 0;
    while(n)
    {
        SaveSubJson(file,n,index);
        n=n->pr_Next;
        index++;
    }
    fprintf(file, "\n\t\t\t%s", "]");

    if (i->it_ObjectTable) {
        if(i->it_ObjectTable->tb_First!=NULL) {
            fprintf(file, "%s\n", ",");
            fprintf(file, "\t\t\t\"ObjectTable\": %s\n", "[");
            SaveTableJson(file,i->it_ObjectTable,0,2);
            fprintf(file, "\n\t\t\t%s", "]");
        }
    }

    if (i->it_SubjectTable) {
        if(i->it_SubjectTable->tb_First!=NULL) {
            fprintf(file, "%s\n", ",");
            fprintf(file, "\t\t\t\"SubjectTable\": %s\n", "[");
            SaveTableJson(file,i->it_SubjectTable,0,2);
            fprintf(file, "\n\t\t\t%s", "]");
        }
    }

    if (i->it_DaemonTable) {
        if(i->it_DaemonTable->tb_First!=NULL) {
            fprintf(file, "%s\n", ",");
            fprintf(file, "\t\t\t\"DaemonTable\": %s\n", "[");
            SaveTableJson(file,i->it_DaemonTable,0,2);
            fprintf(file, "\n\t\t\t%s", "]");
        }
    }

    fprintf(file, "\n\t\t%s", "}");
}

static int is_room(i)
register ITEM *i;
{
    SUB *n;
    KillEventQueue(i);
    n=i->it_Properties;
    while(n)
    {
        if (n->pr_Key == KEY_ROOM)
            return 1;
        n=n->pr_Next;
    }
    return 0;
}

static unsigned long indirect_room_ID(i)
register ITEM *i;
{
    if(i->it_Parent!=NULL)
        if(is_room(i->it_Parent)!=0)
            return i->it_Parent->it_ID;

    return((unsigned long)0);
}

static void SaveRoomJson(file,i,ID)
register FILE *file;
register ITEM *i;
unsigned long ID;
{
    SUB *n;
    unsigned int index;
    unsigned char desc=0,exits=0;

    if(RoomExits(i)==(unsigned int)0)
        return;

    KillEventQueue(i);
    n=i->it_Properties;
    index = 0;
    while(n)
    {
        if ((n->pr_Key == KEY_GENEXIT) || (n->pr_Key == KEY_ROOM)) {
            if (n->pr_Key == KEY_GENEXIT)
                exits=1;
            else
                desc=1;
            if (SaveRoomSubJson(file,n,index,ID,desc,exits)!=0)
                exits=0;
            if ((exits==1) && (desc==1)) {
                exits=desc=0;
            }
        }
        n=n->pr_Next;
        index++;
    }
    if ((exits==0) && (desc==1)) {
        fprintf(file, "\n    %s,\n", "}");
    }
}

static int is_player(i)
register ITEM *i;
{
    SUB *n;
    unsigned int player_exists=0,inouthere_exists=0;
    KillEventQueue(i);
    n=i->it_Properties;
    while(n)
    {
        if (n->pr_Key == KEY_PLAYER)
            player_exists=1;

        if (n->pr_Key == KEY_INOUTHERE)
            inouthere_exists=1;

        if ((player_exists==1) && (inouthere_exists==1))
            return 1;

        n=n->pr_Next;
    }
    return 0;
}

static void SavePlayerJson(file,i,ID,eventfile)
register FILE *file;
register ITEM *i;
unsigned long ID;
register FILE *eventfile;
{
    SUB *n;
    unsigned int index;
    unsigned char playerstats=0,inoutthere=0;

    if(is_player(i)==0)
        return;

    KillEventQueue(i);
    n=i->it_Properties;
    char *name = TextOfWithoutQuotes(i->it_Name);
    unsigned long roomID = 0;
    if (i->it_Parent!=NULL) {
        if(RoomExits(i->it_Parent)==(unsigned int)0)
            return;
        roomID = i->it_Parent->it_ID;
    }
    index = 0;
    while(n)
    {
        if ((n->pr_Key == KEY_INOUTHERE) || (n->pr_Key == KEY_PLAYER)) {
            if (n->pr_Key == KEY_INOUTHERE)
                inoutthere=1;
            else
                playerstats=1;
            if (SavePlayerSubJson(file,n,index,ID,playerstats,inoutthere,name,eventfile,roomID)!=0)
                inoutthere=0;
            if ((inoutthere==1) && (playerstats==1)) {
                inoutthere=playerstats=0;
            }
        }
        n=n->pr_Next;
        index++;
    }
    if ((inoutthere==0) && (playerstats==1)) {
        fprintf(file, "\n    %s,\n", "}");
    }
}

static void SaveItemJson(file,i,ID,eventfile)
register FILE *file;
register ITEM *i;
unsigned long ID;
register FILE *eventfile;
{
    SUB *n;
    unsigned int index;
    KillEventQueue(i);
    n=i->it_Properties;
    unsigned long roomID = 0;
    if (i->it_Parent!=NULL) {
        if(RoomExits(i->it_Parent)==(unsigned int)0)
            return;
        roomID = i->it_Parent->it_ID;
    }
    char *name = TextOfWithoutQuotes(i->it_Name);
    index = 0;
    while(n)
    {
        if (n->pr_Key == KEY_OBJECT) {
            SaveItemSubJson(file,n,index,ID,name,eventfile,roomID);
        }
        n=n->pr_Next;
        index++;
    }
}

static size_t SaveBinaryToFile(ptr, size, n, fp)
const void *ptr;
size_t size;
size_t n;
FILE *fp;
{
    return fwrite(ptr,size,n,fp);
}

#ifdef READ_BINARY_FORMAT
static size_t ReadBinaryFromFile(ptr, size, n, fp)
void *ptr;
size_t size;
size_t n;
FILE *fp;
{
    return fread(ptr,size,n,fp);
}
#endif

static void SetTwo(x,v)
unsigned short *x;
register char *v;
{
    x[1]=((unsigned long)v)%65536;  /* Safe -we never extract this pointer */
    x[0]=((unsigned long)v)/65536;  /* directly so little endians should */
                    /* be just fine.... CHANGE TO HTONS?? */
}

static long GetTwo(x)
register unsigned short *x;
{
    return(x[1]+65536L*x[0]);
}

static void SaveShort(file,v)
FILE *file;
register unsigned short v;
{
/* Assumes 16 bit short - they are used as such even if bigger so is ok */
    unsigned char x[3];
    x[0]=v/256;
    x[1]=v%256;
    if(SaveBinaryToFile(x,2,1,file)!=1)
        Load_Error=errno;
}

static unsigned short LoadShort(x)
FILE *x;
{
#ifndef READ_BINARY_FORMAT
    unsigned short v=0;
    if (fscanf(x,"%hu\n",&v) <= 0)
        Load_Error=errno;
    return(v);
#else
    unsigned char a[3];
    if(ReadBinaryFromFile(a,2,1,x)!=1)
        Load_Error=errno;
    return((unsigned short)(256L*a[0]+a[1]));
#endif
}

static void SaveLong(file,x)
FILE *file;
register unsigned long x;
{
/*
 *  Assumes 32 bits - see notes above
 */
    SaveShort(file,(unsigned short)(x/65536));
    SaveShort(file,(unsigned short)(x%65536));
}

static unsigned long LoadLong(f)
FILE *f;
{
#ifndef READ_BINARY_FORMAT
    unsigned long a;
    fscanf(f,"%lu\n", &a);
    return(a);
#else
    unsigned short a=LoadShort(f);
    return((unsigned long)(a*65536L+LoadShort(f)));
#endif
}

static void SaveItem(file,i)
FILE *file;
ITEM *i;
{
    if(i==NULL)
        SaveLong(file,-1L);
    else
    {
        SaveLong(file,MasterNumber(i));
    }
}

static ITEM *LoadItem(file)
FILE *file;
{
    register long x=LoadLong(file);
    if(x==-1)
        return(NULL);
    if(x>10000) {
        return(NULL);
    }
    return(ItemArray[x]);
}

static void SaveString(file,s)
FILE *file;
TPTR s;
{
    register char *str=TextOf(s);
    register long v=strlen(str);
    if(s->te_Users==1)
        v|=65536;

    SaveLong(file,v);
    if(SaveBinaryToFile(str,strlen(str),1,file)!=1)
        Load_Error=errno;
}

static TPTR LoadString(file)
FILE *file;
{
    register char *a;
#ifndef READ_BINARY_FORMAT
    register long i;
    char c;
#endif
    register long v=LoadLong(file);
    register TPTR t;
    register short f=0;
    if(v>=65536)
    {
        f=1;
        v-=65536;
    }
    a=malloc(v+1);
    if(!a)
    {
        Error("Out Of Memory");
    }

#ifndef READ_BINARY_FORMAT
    for (i = 0; i < v; i++) {
        if (fscanf(file,"%c",&c) <= 0) {
            Load_Error=errno;
        }
        a[i] = c;
    }
#else
    if(ReadBinaryFromFile(a,v,1,file)!=1)
    {
        Load_Error=errno;
    }
#endif

    a[v]=0;
    if(f)
        t=QuickAllocText(a);
    else
        t=AllocText(a);
    free(a);
    return(t);
}

static TPTR LoadComment(file)
FILE *file;
{
    register char *a;
#ifndef READ_BINARY_FORMAT
    register long i;
    char c;
#endif
    register long v=LoadLong(file);
    register TPTR t;
    if(v>65536)
    {
        v-=65536;
    }
    a=malloc(v+1);
    if(!a)
        Error("Out Of Memory");

#ifndef READ_BINARY_FORMAT
    for (i = 0; i < v; i++) {
        if (fscanf(file,"%c",&c) <= 0)
            Load_Error=errno;
        a[i] = c;
    }
#else
    if(ReadBinaryFromFile(a,v,1,file)!=1)
    {
        Load_Error=errno;
    }
#endif

    a[v]=0;
    t=AllocComment(a);
    free(a);
    return(t);
}

static void SaveBSX(file,i)
FILE *file;
BSXImage *i;
{
    int v=strlen(i->bsx_Identifier);
    SaveLong(file,v);
    SaveLong(file,i->bsx_DataSize);

    if(SaveBinaryToFile(i->bsx_Identifier,v,1,file)!=1)
        Load_Error=errno;
    if(SaveBinaryToFile(i->bsx_Data,i->bsx_DataSize,1,file)!=1)
        Load_Error=errno;
}

static BSXImage *LoadBSX(file)
FILE *file;
{
    extern BSXImage *BSXAllocate();
    BSXImage *i;
    char buf[128];
    long v=LoadLong(file);
    long d=LoadLong(file);

#ifndef READ_BINARY_FORMAT
    char c;
    long j;
    for (j = 0; j < v; j++) {
        if (fscanf(file,"%c",&c) <= 0) {
            Load_Error=errno;
            return(NULL);
        }
        buf[j] = c;
    }
#else
    if(ReadBinaryFromFile(buf,v,1,file)!=1)
    {
        Load_Error=errno;
        return(NULL);
    }
#endif

    buf[v]=0;
    i=BSXAllocate(buf,d);

#ifndef READ_BINARY_FORMAT
    for (j = 0; j < d; j++) {
        if (fscanf(file,"%c",&c) <= 0) {
            Load_Error=errno;
            BSXDelete(i);
            return(NULL);
        }
        i->bsx_Data[j] = c;
    }
#else
    if(ReadBinaryFromFile(i->bsx_Data,d,1,file)!=1)
    {
        Load_Error=errno;
        BSXDelete(i);
        return(NULL);
    }
#endif
    return(i);
}

static void SaveWord(file,x)
FILE *file;
register WLIST *x;
{
/* NOTE: Words aren't allowed to contain spaces - THIS RELIES ON IT !!! */
    fprintf(file,"%s %d %d.",x->wd_Text,x->wd_Code,x->wd_Type);
}

static int LoadWord(file)
FILE *file;
{
    static char x[128];
    int a,b;
    if(fscanf(file,"%s %d %d.",x,&a,&b)!=3)
        Error("LoadWord: Corruption In Database File");
    if(strcmp(x,";END")==0) /*; not legal in word either! */
        return(0);
    AddWord(x,(short)a,(short)b);
    return(1);
}

static void SaveVocab(file)
FILE *file;
{
    register WLIST *a=WordList;
    while(a)
    {
        SaveWord(file,a);
        a=a->wd_Next;
    }
    fprintf(file,";END 0 0.");
}

static void LoadVocab(file)
FILE *file;
{
    while(LoadWord(file));
}

static void WriteHeader(file)
FILE *file;
{
    long header[4];
    header[0]=CountItems();
    header[1]=12;       /* Format Identifier */
/*
 *  FORMATS:    0:  Original (no longer supported)
 *              1:  5.04 no flag names
 *              2:  5.04 flag names
 *              3:  5.05 vocab seek & classes
 *              4:  5.06 with text speed flags
 *              5:  5.07 with named tables
 *              6:  5.08 with item bound tables
 *              7:  5.11 USERFLAG2 and CONDEXITs
 *              8:  5.14
 *              9:  5.20 superclass and daemon table
 *             10:  5.21 BSX picture data
 *             11:  5.30
 *             12:  6.00
 */
    time(&(header[2])); /* Save time */
    header[3]=0;        /* Reserved for vocab seek*/
    SaveLong(file,header[0]);
    SaveLong(file,header[1]);
    SaveLong(file,header[2]);
    SaveLong(file,header[3]);
}


static long ReadHeader(file)
FILE *file;
{
    long v[4];
    register long ct;
    v[0]=LoadLong(file);
    v[1]=LoadLong(file);
    v[2]=LoadLong(file);
    v[3]=LoadLong(file);
    Load_Format=v[1];
    if(Load_Format<1)
    {
        fprintf(stderr,
"This database format is too old.\n");
        exit(0);
    }
    if(Load_Format>12)
    {
        fprintf(stderr,
"This database format is beyond my knowledge, you need a newer SERVER.\n");
        exit(0);
    }
    printf("Header Read: %ld Items",v[0]);
    Log("Header Read: %ld Items",v[0]);
    ct=0;
/*
 *  The +1 here stops a 0 sized malloc, which upsets some machines
 */
    ItemArray=(ITEM **)malloc(v[0]*sizeof(ITEM *)+1);
    if(ItemArray==NULL)
        Error("Out Of Memory");
    while(ct<v[0])
    {
        ItemArray[ct]=Allocate(ITEM);
        ItemArray[ct]->it_MasterNext=NULL;
        if(ct) {
            ItemArray[ct-1]->it_MasterNext=ItemArray[ct];
        }
        else {
            ItemList=ItemArray[ct];
        }
        ct++;
    }
    if(ct>0)
        ItemArray[ct-1]->it_MasterNext=NULL;
/*
 *  The above creates a correctly ordered linked set of trash items
 */
    return(v[0]);
}

static void SaveSub(file,s)
FILE *file;
register SUB *s;
{
    SaveShort(file,s->pr_Key);
    switch(s->pr_Key)
    {
        case KEY_INOUTHERE:
            SaveString(file,((INOUTHERE *)s)->io_InMsg);
            SaveString(file,((INOUTHERE *)s)->io_OutMsg);
            SaveString(file,((INOUTHERE *)s)->io_HereMsg);
            break;
        case KEY_USERFLAG2:
        case KEY_USERFLAG:
            SaveShort(file,((USERFLAG *)s)->uf_Flags[0]);
            SaveShort(file,((USERFLAG *)s)->uf_Flags[1]);
            SaveShort(file,((USERFLAG *)s)->uf_Flags[2]);
            SaveShort(file,((USERFLAG *)s)->uf_Flags[3]);
            SaveShort(file,((USERFLAG *)s)->uf_Flags[4]);
            SaveShort(file,((USERFLAG *)s)->uf_Flags[5]);
            SaveShort(file,((USERFLAG *)s)->uf_Flags[6]);
            SaveShort(file,((USERFLAG *)s)->uf_Flags[7]);
            SaveItem(file,((USERFLAG *)s)->uf_Items[0]);
            SaveItem(file,((USERFLAG *)s)->uf_Items[1]);
            SaveItem(file,((USERFLAG *)s)->uf_Items[2]);
            SaveItem(file,((USERFLAG *)s)->uf_Items[3]);
            SaveItem(file,((USERFLAG *)s)->uf_Items[4]);
            SaveItem(file,((USERFLAG *)s)->uf_Items[5]);
            SaveItem(file,((USERFLAG *)s)->uf_Items[6]);
            SaveItem(file,((USERFLAG *)s)->uf_Items[7]);
            break;
        case KEY_CONTAINER:
            SaveShort(file,((CONTAINER *)s)->co_Volume);
            SaveShort(file,((CONTAINER *)s)->co_Flags);
            break;
        case KEY_CHAIN:
            SaveItem(file,((CHAIN *)s)->ch_Chained);
            break;
        case KEY_ROOM:
            SaveString(file,((ROOM *)s)->rm_Short);
            SaveString(file,((ROOM *)s)->rm_Long);
            SaveShort(file,((ROOM *)s)->rm_Flags);
            break;
        case KEY_OBJECT:
            SaveString(file,((OBJECT *)s)->ob_Text[0]);
            SaveString(file,((OBJECT *)s)->ob_Text[1]);
            SaveString(file,((OBJECT *)s)->ob_Text[2]);
            SaveString(file,((OBJECT *)s)->ob_Text[3]);
            SaveShort(file,((OBJECT *)s)->ob_Size);
            SaveShort(file,((OBJECT *)s)->ob_Weight);
            SaveShort(file,((OBJECT *)s)->ob_Flags);
            break;
        case KEY_PLAYER:
            SaveShort(file,((PLAYER *)s)->pl_UserKey);
            SaveShort(file,((PLAYER *)s)->pl_Size);
            SaveShort(file,((PLAYER *)s)->pl_Weight);
            SaveShort(file,((PLAYER *)s)->pl_Strength);
            SaveShort(file,((PLAYER *)s)->pl_Flags);
            SaveShort(file,((PLAYER *)s)->pl_Level);
            SaveLong(file,((PLAYER *)s)->pl_Score);
            break;
        case KEY_GENEXIT:
            SaveItem(file,((GENEXIT *)s)->ge_Dest[0]);
            SaveItem(file,((GENEXIT *)s)->ge_Dest[1]);
            SaveItem(file,((GENEXIT *)s)->ge_Dest[2]);
            SaveItem(file,((GENEXIT *)s)->ge_Dest[3]);
            SaveItem(file,((GENEXIT *)s)->ge_Dest[4]);
            SaveItem(file,((GENEXIT *)s)->ge_Dest[5]);
            SaveItem(file,((GENEXIT *)s)->ge_Dest[6]);
            SaveItem(file,((GENEXIT *)s)->ge_Dest[7]);
            SaveItem(file,((GENEXIT *)s)->ge_Dest[8]);
            SaveItem(file,((GENEXIT *)s)->ge_Dest[9]);
            SaveItem(file,((GENEXIT *)s)->ge_Dest[10]);
            SaveItem(file,((GENEXIT *)s)->ge_Dest[11]);
            break;
        case KEY_CONDEXIT:
            SaveItem(file,((CONDEXIT *)s)->ce_Dest);
            SaveShort(file,(short)((CONDEXIT *)s)->ce_Table);
            SaveShort(file,(short)((CONDEXIT *)s)->ce_ExitNumber);
            break;
        case KEY_MSGEXIT:
            SaveItem(file,((MSGEXIT *)s)->me_Dest);
            SaveString(file,((MSGEXIT *)s)->me_Text);
            SaveShort(file,((MSGEXIT *)s)->me_ExitNumber);
            break;
        case KEY_INHERIT:
            SaveItem(file,((INHERIT *)s)->in_Master);
            break;
        case KEY_DUPED:
            SaveItem(file,((DUP *)s)->du_Master);
            break;
        case KEY_USERTEXT:
            SaveString(file,((USERTEXT *)s)->ut_Text[0]);
            SaveString(file,((USERTEXT *)s)->ut_Text[1]);
            SaveString(file,((USERTEXT *)s)->ut_Text[2]);
            SaveString(file,((USERTEXT *)s)->ut_Text[3]);
            SaveString(file,((USERTEXT *)s)->ut_Text[4]);
            SaveString(file,((USERTEXT *)s)->ut_Text[5]);
            SaveString(file,((USERTEXT *)s)->ut_Text[6]);
            SaveString(file,((USERTEXT *)s)->ut_Text[7]);
            break;
        default:Error("Unknown Subtype");
    }
}

static void LoadSub(file,item,type)
FILE *file;
ITEM *item;
short type;
{
    TPTR xt;
    switch(type)
    {
        case KEY_INOUTHERE:
        {
            SetInMsg(item,TextOf(xt=LoadString(file)));
            FreeText(xt);
            SetOutMsg(item,TextOf(xt=LoadString(file)));
            FreeText(xt);
            SetHereMsg(item,TextOf(xt=LoadString(file)));
            FreeText(xt);
            break;
        }
        case KEY_USERTEXT:
        {
            SetUText(item,0,(xt=LoadString(file)));
            FreeText(xt);
            SetUText(item,1,(xt=LoadString(file)));
            FreeText(xt);
            SetUText(item,2,(xt=LoadString(file)));
            FreeText(xt);
            SetUText(item,3,(xt=LoadString(file)));
            FreeText(xt);
            SetUText(item,4,(xt=LoadString(file)));
            FreeText(xt);
            SetUText(item,5,(xt=LoadString(file)));
            FreeText(xt);
            SetUText(item,6,(xt=LoadString(file)));
            FreeText(xt);
            SetUText(item,7,(xt=LoadString(file)));
            FreeText(xt);
            break;
        }
        case KEY_CONTAINER:
        {
            register CONTAINER *c=BeContainer(item);
            c->co_Volume=LoadShort(file);
            c->co_Flags=LoadShort(file);
            break;
        }
        case KEY_USERFLAG:
        case KEY_USERFLAG2:
        {
            register USERFLAG *u;
            int x=0;
            if(type==KEY_USERFLAG2)
                x=8;
            SetUserFlag(item,0+x,LoadShort(file));
            SetUserFlag(item,1+x,LoadShort(file));
            SetUserFlag(item,2+x,LoadShort(file));
            SetUserFlag(item,3+x,LoadShort(file));
            SetUserFlag(item,4+x,LoadShort(file));
            SetUserFlag(item,5+x,LoadShort(file));
            SetUserFlag(item,6+x,LoadShort(file));
            SetUserFlag(item,7+x,LoadShort(file));
            if(type==KEY_USERFLAG)
                u=UserFlagOf(item);
            else
                u=UserFlag2Of(item);
            u->uf_Items[0]=LoadItem(file);
            u->uf_Items[1]=LoadItem(file);
            u->uf_Items[2]=LoadItem(file);
            u->uf_Items[3]=LoadItem(file);
            if(Load_Format>7)
            {
                u->uf_Items[4]=LoadItem(file);
                u->uf_Items[5]=LoadItem(file);
                u->uf_Items[6]=LoadItem(file);
                u->uf_Items[7]=LoadItem(file);
            }
            break;
        }
        case KEY_CHAIN:
        {
            ITEM *x=LoadItem(file);
            AddNLChain(item,x);
            break;
        }
        case KEY_ROOM:
        {
            register ROOM *a;
            MakeRoom(item);
            a=(ROOM *)FindSub(item,KEY_ROOM);
            FreeText(a->rm_Short);
            FreeText(a->rm_Long);
            a->rm_Short=LoadString(file);
            a->rm_Long=LoadString(file);
            a->rm_Flags=LoadShort(file);
            break;
        }
        case KEY_OBJECT:
        {
            register OBJECT *o;
            MakeObject(item);
            o=(OBJECT *)FindSub(item,KEY_OBJECT);
            FreeText(o->ob_Text[0]);
            FreeText(o->ob_Text[1]);
            FreeText(o->ob_Text[2]);
            FreeText(o->ob_Text[3]);
            o->ob_Text[0]=LoadString(file);
            o->ob_Text[1]=LoadString(file);
            o->ob_Text[2]=LoadString(file);
            o->ob_Text[3]=LoadString(file);
            o->ob_Size=LoadShort(file);
            o->ob_Weight=LoadShort(file);
            o->ob_Flags=LoadShort(file);
            break;
        }
        case KEY_PLAYER:
        {
            register PLAYER *p;
            MakePlayer(item);
            p=(PLAYER *)FindSub(item,KEY_PLAYER);
            p->pl_UserKey=LoadShort(file);
            p->pl_Size=LoadShort(file);
            p->pl_Weight=LoadShort(file);
            p->pl_Strength=LoadShort(file);
            p->pl_Flags=LoadShort(file);
            p->pl_Level=LoadShort(file);
            p->pl_Score=LoadLong(file);
            break;
        }
        case KEY_GENEXIT:
        {
            register GENEXIT *g;
            MakeGenExit(item);
            g=(GENEXIT *)FindSub(item,KEY_GENEXIT);
            g->ge_Dest[0]=LoadItem(file);
            g->ge_Dest[1]=LoadItem(file);
            g->ge_Dest[2]=LoadItem(file);
            g->ge_Dest[3]=LoadItem(file);
            g->ge_Dest[4]=LoadItem(file);
            g->ge_Dest[5]=LoadItem(file);
            g->ge_Dest[6]=LoadItem(file);
            g->ge_Dest[7]=LoadItem(file);
            g->ge_Dest[8]=LoadItem(file);
            g->ge_Dest[9]=LoadItem(file);
            g->ge_Dest[10]=LoadItem(file);
            g->ge_Dest[11]=LoadItem(file);
            break;
        }
        case KEY_CONDEXIT:
        {
            register ITEM *a;
            short c,d;
            a=LoadItem(file);
            c=LoadShort(file);
            d=LoadShort(file);
            /* MakeCond... sets its own locks */
            MakeNLCondExit(item,a,c,d);
            break;
        }
        case KEY_MSGEXIT:
        {
            ITEM *a;
            TPTR b;
            short c;
            a=LoadItem(file);
            b=LoadString(file);
            c=LoadShort(file);
            MakeNLMsgExit(item,a,c,TextOf(b));
            FreeText(b);
            break;
        }
        case KEY_INHERIT:
        {
            ITEM *a=LoadItem(file);
            register INHERIT *d=(INHERIT *)AllocSub(item,KEY_INHERIT,
                sizeof(INHERIT));
            d->in_Master=a;
            break;
        }
        case KEY_DUPED:
        {
            ITEM *a=LoadItem(file);
            register DUP *d=(DUP *)AllocSub(item,KEY_DUPED,sizeof(DUP));
            d->du_Master=a;
            break;
        }
        default:Log("Subtype %d found!",type);
            Error("Unknown Subtype");
    }
}

static void LoadObject(file,i)
register FILE *file;
register ITEM *i;
{
    register long n;
    i->it_Name=LoadString(file);
/*  printf("%s\n",TextOf(i->it_Name));  */
    i->it_Adjective=LoadShort(file);
    i->it_Noun=LoadShort(file);
    i->it_State=LoadShort(file);
    i->it_Perception=LoadShort(file);
    i->it_Next=LoadItem(file);
    i->it_Children=LoadItem(file);
    i->it_Parent=LoadItem(file);
    i->it_ActorTable=LoadShort(file);
    i->it_ActionTable=LoadShort(file);
    i->it_Users=LoadShort(file);
    i->it_Class=LoadShort(file);
    if(Load_Format>8)
    {
        if(LoadShort(file))
            i->it_Superclass=LoadItem(file);
        else
            i->it_Superclass=NULL;
    }
    i->it_Properties=NULL;
    n=LoadLong(file);   /* props */
    if(n==0)
        return;
    while(n)
    {
        n=LoadShort(file);
        if(n)
            LoadSub(file,i,(short)n);
    }
    if(Load_Format>5)
    {
        i->it_ObjectTable=LoadItemTable(file);
        i->it_SubjectTable=LoadItemTable(file);
        if(Load_Format>8)
            i->it_DaemonTable=LoadItemTable(file);
        else
            i->it_DaemonTable=NULL;
    }
    else
    {
        i->it_ObjectTable=NULL;
        i->it_SubjectTable=NULL;
    }
}

static void SaveObject(file,i)
register FILE *file;
register ITEM *i;
{
    SUB *n;
    KillEventQueue(i);
    SaveString(file,i->it_Name);
    SaveShort(file,i->it_Adjective);
    SaveShort(file,i->it_Noun);
    SaveShort(file,i->it_State);
    SaveShort(file,i->it_Perception);
    SaveItem(file,i->it_Next);
    SaveItem(file,i->it_Children);
    SaveItem(file,i->it_Parent);
    SaveShort(file,i->it_ActorTable);
    SaveShort(file,i->it_ActionTable);
    SaveShort(file,i->it_Users);
    SaveShort(file,i->it_Class);
    if(i->it_Superclass)
    {
        SaveShort(file,1);
        SaveItem(file,i->it_Superclass);
    }
    else
        SaveShort(file,0);
    SaveLong(file,(unsigned long)i->it_Properties);
    n=i->it_Properties;
    if(n==0)
        return;
    while(n)
    {
        SaveSub(file,n);
        n=n->pr_Next;
    }
    SaveShort(file,0);  /* Prop end marker */
    SaveItemTable(file,i->it_ObjectTable);
    SaveItemTable(file,i->it_SubjectTable);
    SaveItemTable(file,i->it_DaemonTable);
}

unsigned short *SaveAction(file,c)
FILE *file;
register unsigned short *c;
{
    int cc=*c;
    TPTR t;
    ITEM *i;
    register char *ptr;
    SaveShort(file,*c++);
    ptr=Cnd_Table[cc];
    while(*ptr!=' ')
    {
        switch(*ptr++)
        {
        case '$':;
        case 'T':t=(TPTR )GetTwo(c);
             c+=2;
             if(t==(TPTR )1)
                SaveShort(file,0);
             else
             {
                if(t==(TPTR)3)
                    SaveShort(file,3);
                else
                {
                    SaveShort(file,1);
                    SaveString(file,t);
                }
             }
             break;
        case 'I':i=(ITEM *)GetTwo(c);
             c+=2;
             if(i==(ITEM *)1)
             {
                SaveShort(file,1);
                break;
             }
             if(i==(ITEM *)3)
             {
                SaveShort(file,3);
                break;
             }
             if(i==(ITEM *)5)
             {
                SaveShort(file,5);
                break;
             }
             if(i==(ITEM *)7)
             {
                SaveShort(file,7);
                break;
             }
             if(i==(ITEM *)9)
             {
                SaveShort(file,9);
                break;
             }
             SaveShort(file,0);
             SaveItem(file,i);
             break;
        case 'C':;
        case 'R':;
        case 'O':;
        case 'P':;
        case 'c':;
        case 'B':;
        case 'F':;
        case 'v':;
        case 'p':;
        case 'n':;
        case 'a':;
        case 't':;
        case 'N':SaveShort(file,*c++);
             break;
        default:Error("Save: Bad Cnd_Table Entry");
        }
    }
    return(c);
}

short *LoadAction(file,c)
FILE *file;
register short *c;
{
    short cc=*c++;
    ITEM *i;
    TPTR t;
    register char *ptr=Cnd_Table[cc];
    while(*ptr!=' ')
    {
        switch(*ptr++)
        {
        case '$':switch(LoadShort(file))
             {
                case 0:t=(TPTR)1;break;
                case 3:t=(TPTR)3;break;
                default:t=LoadComment(file);
             }
             SetTwo(c,(char *)t);
             c+=2;
             break;
        case 'T':switch(LoadShort(file))
             {
                case 0:t=(TPTR )1;break;
                case 3:t=(TPTR) 3;break;
                default:t=LoadString(file);
             }
             SetTwo(c,(char *)t);
             c+=2;
             break;
        case 'I':switch(LoadShort(file))
             {
                case 1:i=(ITEM *)1;break;
                case 3:i=(ITEM *)3;break;
                case 5:i=(ITEM *)5;break;
                case 7:i=(ITEM *)7;break;
                case 9:i=(ITEM *)9;break;
                default:i=LoadItem(file);
             }
             SetTwo(c,(char *)i);
             c+=2;
             break;
        case 'B':;
        case 'C':;
        case 'c':;
        case 'R':;
        case 'O':;
        case 'P':;
        case 'F':;
        case 'v':;
        case 'p':;
        case 'n':;
        case 'a':;
        case 't':;
        case 'N':*c++=LoadShort(file);
             break;
        default:Error("Load: Bad Cnd_Table Entry");
        }
    }
    return(c);
}

void LoadLine(file,l)
FILE *file;
register LINE *l;
{
    short *a=(short *)malloc(1024);
    register short *b=a;
    if(a==NULL)
        Error("Out Of Memory");
    l->li_Verb=LoadShort(file);
    l->li_Noun1=LoadShort(file);
    l->li_Noun2=LoadShort(file);
    while((*b=LoadShort(file))!=CMD_EOL)
    {
        b=LoadAction(file,b);
    }
    free(l->li_Data);
    l->li_Data=(unsigned short *)malloc(sizeof(short)*(b-a+2));
    memcpy((char *)l->li_Data,(char *)a,sizeof(short)*(b-a+2));
    free((char *)a);
}

void SaveLine(file,l)
FILE *file;
LINE *l;
{
    unsigned short *a=l->li_Data;
    SaveShort(file,l->li_Verb);
    SaveShort(file,l->li_Noun1);
    SaveShort(file,l->li_Noun2);
    while(*a!=CMD_EOL)
    {
        a=SaveAction(file,a);
    }
    SaveShort(file,CMD_EOL);
}

TABLE *LoadItemTable(file)
FILE *file;
{
    TABLE *t;
    int x=LoadShort(file);
    if(x==0)
        return(NULL);
    t=Allocate(TABLE);
    LoadShort(file);
    LoadTable(file,t);
    return(t);
}

void SaveItemTable(file,t)
FILE *file;
TABLE *t;
{
    if(t==NULL)
    {
        SaveShort(file,0);
    }
    else
    {
        SaveShort(file,1);
        SaveTable(file,t);
    }
}

void LoadTable(file,t)
FILE *file;
TABLE *t;
{
    register LINE *l;
    if(Load_Format>4)
        t->tb_Name=LoadString(file);
    else
        t->tb_Name=AllocText("(unset)");
    while(LoadShort(file)==0)
    {
        l=NewLine(t,65535);
        LoadLine(file,l);
    }
}

void SaveTable(file,t)
FILE *file;
TABLE *t;
{
    register LINE *l=t->tb_First;
    SaveShort(file,t->tb_Number);
    SaveString(file,t->tb_Name);
    while(l)
    {
        SaveShort(file,0);
        SaveLine(file,l);
        l=l->li_Next;
    }
    SaveShort(file,1);
}

void LoadAllTables(file)
FILE *file;
{
    register TABLE *t;
    while(LoadShort(file)==0)
    {
        t=NewTable(LoadShort(file),NULL);
        LoadTable(file,t);
    }
}

void SaveAllTables(file)
FILE *file;
{
    register TABLE *t=TableList;
    while(t)
    {
        SaveShort(file,0);
        SaveTable(file,t);
        t=t->tb_Next;
    }
    SaveShort(file,1);
}

extern char *FlagName[];

static void SaveFlag(file,s)
FILE *file;
short s;
{
    register char *str=FlagName[s];
    short v;
    if(str==NULL)
        SaveShort(file,0);
    else
    {
        v=strlen(str);
/*      printf("%d %ld\n",(int)s,v);    */
        SaveShort(file,v);
        if(SaveBinaryToFile(str,strlen(str),1,file)!=1)
            Load_Error=errno;
    }
}


void LoadFlag(file,n)
FILE *file;
register short n;
{
    register char *a;
#ifndef READ_BINARY_FORMAT
    long i;
    char c;
#endif
    long v=LoadShort(file);
/*  printf("FLAG %d V=%ld",(int)n,v);   */
    if(v==0)
    {
        SetFlagName(n,NULL);
    }
    else
    {
        a=malloc(v+1);
        if(!a)
            Error("Out Of Memory");

#ifndef READ_BINARY_FORMAT
        for (i = 0; i < v; i++) {
            if (fscanf(file,"%c",&c) <= 0)
                Load_Error=errno;
            a[i] = c;
        }
#else
        if(ReadBinaryFromFile(a,v,1,file)!=1)
            Load_Error=errno;
#endif

        a[v]=0;
        SetFlagName(n,a);
        free(a);
    }
}

static void LoadClass(a,c)
FILE *a;
short c;
{
    if(LoadShort(a))
        SetClassTxt(c,LoadString(a));
    else
        SetClassName(c,NULL);
}

static void SaveClass(a,c)
FILE *a;
short c;
{
    if(GetClassTxt(c))
    {
        SaveShort(a,1);
        SaveString(a,GetClassTxt(c));
    }
    else
        SaveShort(a,0);
}

void SaveBitFlags(f)
FILE *f;
{
    int ct=0;
    while(ct<16)
    {
        fprintf(f,"%s\001",RBitName(ct));
        fprintf(f,"%s\001",OBitName(ct));
        fprintf(f,"%s\001",PBitName(ct));
        fprintf(f,"%s\001",CBitName(ct));
        ct++;
    }
}

static char *LoadDupStr(x)
char *x;
{
    char *a=strdup(x);
    if(!a)
    {
        fprintf(stderr,"Out Of Memory");
        exit(0);
    }
    return(a);
}

void LodBuf(f,x)
FILE *f;
char *x;
{
    while((*x=fgetc(f))!='\001')
        x++;
    *x=0;
}

void LoadBitFlags(f)
FILE *f;
{
    int ct=0;
    char buf[256];
    while(ct<16)
    {
        LodBuf(f,buf);
        RBitNames[ct]=LoadDupStr(buf);
        LodBuf(f,buf);
        OBitNames[ct]=LoadDupStr(buf);
        LodBuf(f,buf);
        PBitNames[ct]=LoadDupStr(buf);
        LodBuf(f,buf);
        CBitNames[ct]=LoadDupStr(buf);
        ct++;
    }
}

void SaveBSXImages(file)
FILE *file;
{
    extern BSXImage *BSXFindFirst(),*BSXFindNext();
    BSXImage *i=BSXFindFirst();
    while(i!=NULL)
    {
        SaveShort(file,1);
        SaveBSX(file,i);
        i=BSXFindNext(i);
    }
    SaveShort(file,0);
}

void LoadBSXImages(file)
FILE *file;
{
    int i;
    i=LoadShort(file);
    while(i!=0)
    {
        LoadBSX(file);
        i=LoadShort(file);
    }
}

int SaveSystem(n)
char *n;
{
    register ITEM *i;
    static char b[140]; /* Max file name length =128 */
    FILE *a=fopen(n,"r");
    register short c=0;
    sprintf(b,"%s.bak",n);
    if(a)
    {
        fclose(a);
        unlink(b);  /* Can't rename over another file */
        if(rename(n,b)==-1)
        {
            return(-1);
        }   /* TRY LINK(a,b) ON UNIX - OR
               SEE /usr/src/bin/passwd.c FOR YOUR CLONE. ANYWAY
               RENAME IS ANSI ...*/
    }
/* Now to save */
    a=fopen(n,"w");
    if(a==NULL)
        return(-2);
    Load_Error=0;
    WriteHeader(a);
    i=ItemList;
    while(i)
    {
        SaveObject(a,i);
        i=i->it_MasterNext;
    }
    SaveAllTables(a);
    SaveVocab(a);
    while(c<512)
        SaveFlag(a,c++);
    c=0;
    while(c<16)
        SaveClass(a,c++);
    SaveBitFlags(a);
    SaveBSXImages(a);
    if(ferror(a)&&Load_Error==0)
        Load_Error=errno;
    fclose(a);
    return(Load_Error);
}

int SaveSystemRoomsJson(n)
char *n;
{
    unsigned long ID;
    register ITEM *i;
    static char b[140]; /* Max file name length =128 */
    FILE *a=fopen(n,"r");
    sprintf(b,"%s.bak",n);
    if(a)
    {
        fclose(a);
        unlink(b);  /* Can't rename over another file */
        if(rename(n,b)==-1)
        {
            return(-1);
        }   /* TRY LINK(a,b) ON UNIX - OR
               SEE /usr/src/bin/passwd.c FOR YOUR CLONE. ANYWAY
               RENAME IS ANSI ...*/
    }
    /* Now to save */
    a=fopen(n,"w");
    if(a==NULL)
        return(-2);
    Load_Error=0;

    fprintf(a,"%s\n","{");

    /* assign ID numbers to each item */
    ID=1;
    i=ItemList;
    while(i)
    {
        i->it_ID=ID++;
        i=i->it_MasterNext;
    }

    /* save items */
    ID=1;
    i=ItemList;
    while(i)
    {
        SaveRoomJson(a,i,ID++);
        /*SaveObjectJson(a,i,ID++);*/
        i=i->it_MasterNext;
    }

    if(ferror(a)&&Load_Error==0)
        Load_Error=errno;

    fprintf(a,"%s\n","}");
    fclose(a);
    return(Load_Error);
}

int SaveSystemPlayersJson(n,events)
char *n;
char *events;
{
    unsigned long ID;
    register ITEM *i;
    static char b[140]; /* Max file name length =128 */
    FILE *a=fopen(n,"r");
    sprintf(b,"%s.bak",n);
    if(a)
    {
        fclose(a);
        unlink(b);  /* Can't rename over another file */
        if(rename(n,b)==-1)
        {
            return(-1);
        }   /* TRY LINK(a,b) ON UNIX - OR
               SEE /usr/src/bin/passwd.c FOR YOUR CLONE. ANYWAY
               RENAME IS ANSI ...*/
    }
    FILE *ev=fopen(events,"w");
    if(ev==NULL)
        return(-2);
    /* Now to save */
    a=fopen(n,"w");
    if(a==NULL) {
        fclose(ev);
        return(-2);
    }
    Load_Error=0;

    fprintf(ev,"%s\n","# Reserved event 2 - Used for spawnNPC. Executed once on server boot, after all assets have been loaded.");
    fprintf(ev,"%s\n","# 0|spawnNPC|id;room;isAttackable;isStealable;isKillable;isAggressive;corpseTTL;respawn");
    fprintf(ev,"%s\n","2");

    fprintf(a,"%s\n","{");

    /* assign ID numbers to each item */
    ID=1;
    i=ItemList;
    while(i)
    {
        i->it_ID=ID++;
        i=i->it_MasterNext;
    }

    /* save items */
    ID=1;
    i=ItemList;
    while(i)
    {
        SavePlayerJson(a,i,ID++,ev);
        i=i->it_MasterNext;
    }

    if(ferror(a)&&Load_Error==0)
        Load_Error=errno;

    fprintf(a,"\n%s\n","}");
    fclose(a);
    fclose(ev);
    return(Load_Error);
}

int SaveSystemItemsJson(n,events)
char *n;
char *events;
{
    unsigned long ID;
    register ITEM *i;
    static char b[140]; /* Max file name length =128 */
    FILE *a=fopen(n,"r");
    sprintf(b,"%s.bak",n);
    if(a)
    {
        fclose(a);
        unlink(b);  /* Can't rename over another file */
        if(rename(n,b)==-1)
        {
            return(-1);
        }   /* TRY LINK(a,b) ON UNIX - OR
               SEE /usr/src/bin/passwd.c FOR YOUR CLONE. ANYWAY
               RENAME IS ANSI ...*/
    }
    FILE *ev=fopen(events,"w");
    if(ev==NULL)
        return(-2);
    /* Now to save */
    a=fopen(n,"w");
    if(a==NULL) {
        fclose(ev);
        return(-2);
    }
    Load_Error=0;

    fprintf(ev,"%s\n","# Reserved event 1 - used for spawnItem");
    fprintf(ev,"%s\n","# Executed once on server boot, after all assets have been loaded");
    fprintf(ev,"%s\n","1");

    fprintf(a,"%s\n","{");

    /* assign ID numbers to each item */
    ID=1;
    i=ItemList;
    while(i)
    {
        i->it_ID=ID++;
        i=i->it_MasterNext;
    }

    /* save items */
    ID=1;
    i=ItemList;
    while(i)
    {
        SaveItemJson(a,i,ID++,ev);
        i=i->it_MasterNext;
    }

    if(ferror(a)&&Load_Error==0)
        Load_Error=errno;

    fprintf(a,"%s\n","}");
    fclose(a);
    fclose(ev);
    return(Load_Error);
}

int LoadSystem(n)
char *n;
{
    FILE *a=fopen(n,"r");
    register long ct=0;
    long v,c=0;
    if(a==NULL)
        return(-1);
    Load_Error=0;
    v=ReadHeader(a);    /* Set up items */
    printf("ct %ld  v%ld\n",ct,v);
    while(ct<v)
    {
        LoadObject(a,ItemArray[ct]);
        ct++;
    }
    LoadAllTables(a);
    free((char *)ItemArray);    /* Free Reloc Info */
    LoadVocab(a);
    if(Load_Format>1)   /* If new format database.. */
        while(c<512)
            LoadFlag(a,c++);
    if(Load_Format>2)   /* If 5.05 dbm load classes */
    {
        c=0;
        while(c<16)
            LoadClass(a,c++);
    }
    if(Load_Format>4)   /* 5.07 load bit flags too */
        LoadBitFlags(a);
    if(Load_Format>9)
        LoadBSXImages(a);
    fclose(a);
    Log("Load Read Completed");
    SaveSystemRoomsJson("rooms.json");
    SaveSystemItemsJson("items.json","1.event");
    SaveSystemPlayersJson("npcs.json","2.event");
    return(Load_Error);
}
