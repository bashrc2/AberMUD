 /****************************************************************************\
 *                                                                            *
 *                    C R E A T O R    O F   L E G E N D S                    *
 *                             (AberMud Version 6)                            *
 *                                                                            *
 *  The Creator Of Legends System is (C) Copyright 1989 Alan Cox, All Rights  *
 *  Reserved.                                                                 *
 *                                                                            *
 *  This program is free software; you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation; either version 2 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program; if not, write to the Free Software               *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
 *                                                                            *
 \****************************************************************************/

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/wait.h>

/*
 *  AberMUD boot up program.
 *
 *  1.00    AGC Prehistory  Original boot up program
 *  1.01    AGC 27/02/93    Passes arguments on
 *
 */

int main(int argc,char *argv[])
{
    long t,t2;
    int pid;
    printf("Aberystwyth Multi-User Dungeon (Revision 5.21 BETA 5)\n\
(c) Copyright 1987-1993, Alan Cox\n\
\n");
    close(0);
    close(1);
    ioctl(2,TIOCNOTTY,0);
    close(2);
    setpgrp();
    if(fork()!=0)
        exit(1);
    open("server_log",O_WRONLY|O_CREAT|O_APPEND,0600);
    dup(0);
    dup(0);
    while(1)
    {
        time(&t);
        argv[0]="AberMUD 5.21 Beta1";
        pid=fork();
        if(pid==0)
        {
            execvp("./server",&argv[0]);
            perror("./server");
            exit(1);
        }
        else
            if(pid!= -1)
                wait(NULL);
        time(&t2);
        if(t2-t<10)
        {
            printf("Spawning too fast - error ??\n");
            exit(1);
        }
    }
}
