 /****************************************************************************\
 *                                                                            *
 *                    C R E A T O R    O F   L E G E N D S                    *
 *                             (AberMud Version 6)                            *
 *                                                                            *
 *  The Creator Of Legends System is (C) Copyright 1989 Alan Cox, All Rights  *
 *  Reserved.                                                                 *
 *                                                                            *
 *  This program is free software; you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation; either version 2 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program; if not, write to the Free Software               *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
 *                                                                            *
 \****************************************************************************/

/*
 *  Set up a socket
 *
 *  Alan Cox 1990
 *
 *  This software is placed in the public domain
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>


/*
 *  Create the client socket
 */

int Make_Connection(int port,const char *buf)
{
    struct sockaddr_in myaddress;
    struct hostent *host;
    int v;
    host=gethostbyname(buf);
    if(host==NULL)
    {
            return(-1);
    }
    myaddress.sin_family=host->h_addrtype;
    myaddress.sin_addr.s_addr=*((long *)host->h_addr);
    myaddress.sin_port=htons(port);
    v=socket(AF_INET,SOCK_STREAM,0);
    if(v==-1)
    {
        return(-1);
    }
    if(connect(v,(struct sockaddr *)&myaddress,sizeof(myaddress))<0)
    {
        close(v);
        return(-1);
    }
    return(v);
}
