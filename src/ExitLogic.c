 /****************************************************************************\
 *                                                                            *
 *                    C R E A T O R    O F   L E G E N D S                    *
 *                             (AberMud Version 6)                            *
 *                                                                            *
 *  The Creator Of Legends System is (C) Copyright 1989 Alan Cox, All Rights  *
 *  Reserved.                                                                 *
 *                                                                            *
 *  This program is free software; you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation; either version 2 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program; if not, write to the Free Software               *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
 *                                                                            *
 \****************************************************************************/

/*
 *  Conditional Exit Logic Evaluator
 *
 *  1.00    AGC Created this file, to replace stub version
 */

#include "System.h"

Module  "Exit Logic";
Version "1.00";
Author  "Alan Cox";


int TestCondExit(e)
CONDEXIT *e;
{
    TABLE *t=FindTable(e->ce_Table);
    if(t!=NULL)
        if(ExecBackground(t,Me())==-1)
            return(0);
    return(1);
}
