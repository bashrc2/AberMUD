 /****************************************************************************\
 *                                                                            *
 *                    C R E A T O R    O F   L E G E N D S                    *
 *                             (AberMud Version 6)                            *
 *                                                                            *
 *  The Creator Of Legends System is (C) Copyright 1989 Alan Cox, All Rights  *
 *  Reserved.                                                                 *
 *                                                                            *
 *  This program is free software; you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation; either version 2 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program; if not, write to the Free Software               *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
 *                                                                            *
 \****************************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define LINUX

#ifndef LINUX

char *strdup(x)
char *x;
{
    extern char *malloc();
    char *t=malloc(strlen(x)+1);
    if(t==NULL)
        return(NULL);
    strcpy(t,x);
    return(t);
}

#endif

int stricmp(a,b)
char *a,*b;
{
    while(*a)
    {
        char x,y;
        x=*a;
        y=*b;
        if(isupper(x)) x=tolower(x);
        if(isupper(y)) y=tolower(y);
        if(x<y)
            return(-1);
        if(x>y)
            return(1);
        a++;
        b++;
    }
    if(*b!=0)
        return(1);
    return(0);
}

#ifndef LINUX

rename(a,b)
char *a,*b;
{
    if(link(a,b)==-1)
        return(-1);
    if(unlink(a)==-1)
        return(-1);
    return(0);
}


#endif

char *strtok2(a,b,c)
char *a,*b,*c;
{
    static char *d,*e;
    if(a!=NULL)
        d=a;
    if(d==NULL)
        return(NULL);
    while(*d&&strchr(b,*d))
    {
        if(*d=='{') /* FUDGE FUDGE... */
        {
            d++;
            break;
        }
        d++;
    }
    if(*d==0)
    {
        d=NULL;
        return(NULL);
    }
    e=d;
    while(*d&&strchr(c,*d)==NULL)
        d++;
    if(*d)
        *d++=0;
    return(e);
}
