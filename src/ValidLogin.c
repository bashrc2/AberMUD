 /****************************************************************************\
 *                                                                            *
 *                    C R E A T O R    O F   L E G E N D S                    *
 *                             (AberMud Version 6)                            *
 *                                                                            *
 *  The Creator Of Legends System is (C) Copyright 1989 Alan Cox, All Rights  *
 *  Reserved.                                                                 *
 *                                                                            *
 *  This program is free software; you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation; either version 2 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program; if not, write to the Free Software               *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
 *                                                                            *
 \****************************************************************************/

/*
 *  This function should be customised according to the actual
 *  load parameters you wish to enforce
 */

#include <time.h>
#include <stdio.h>

int MaxSlot(void)
{
    long t;
    struct tm *tv;
    time(&t);
    tv=localtime(&t);
    if(tv==NULL)
        return(0);
    if(tv->tm_hour<7)
        return(20);
    if(tv->tm_hour<21)
        return(12);
    if(tv->tm_hour<23)
        return(16);
    if(tv->tm_hour==23)
        return(20);
    return(8);
}
