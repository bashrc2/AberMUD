 /****************************************************************************\
 *                                                                            *
 *                    C R E A T O R    O F   L E G E N D S                    *
 *                             (AberMud Version 6)                            *
 *                                                                            *
 *  The Creator Of Legends System is (C) Copyright 1989 Alan Cox, All Rights  *
 *  Reserved.                                                                 *
 *                                                                            *
 *  This program is free software; you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation; either version 2 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program; if not, write to the Free Software               *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
 *                                                                            *
 \****************************************************************************/

/*
 *  Extra functions to get the Amiga version working nicely
 */

#include <stdio.h>
#include <pragmas/socket_pragmas.h>
extern long SocketBase;


long htonl(long x){return x;}
long ntohl(long x){return x;}
short htons(short x){return x;}
short ntohs(short x){return x;}

void bcopy(void *a,void *b,int c)
{
    memcpy(b,a,c);
}

void bzero(void *p,int n)
{
    memset(p,'\0',n);
}

void sleep(int n)
{
    Delay(50*n);
}

#ifndef MUPX
int gethostname(char *name,int namelen)
{
    char *cp=getenv("HOSTNAME");
    if(cp==NULL)
        return(-1);
    stccpy(name,cp,namelen);
    free(cp);
    return 0;
}
#endif

int select(int nfds,void *readmask,void *writemask, void *exceptmask, void *timeout)
{
    return WaitSelect(nfds,readmask,writemask,exceptmask,timeout,NULL);
}
