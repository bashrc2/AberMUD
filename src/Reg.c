 /****************************************************************************\
 *                                                                            *
 *                    C R E A T O R    O F   L E G E N D S                    *
 *                             (AberMud Version 6)                            *
 *                                                                            *
 *  The Creator Of Legends System is (C) Copyright 1989 Alan Cox, All Rights  *
 *  Reserved.                                                                 *
 *                                                                            *
 *  This program is free software; you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation; either version 2 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program; if not, write to the Free Software               *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
 *                                                                            *
 \****************************************************************************/

#include "System.h"

int main(int argc,char *argv[])
{
    short ct=1;
    UFF u;
    while(argv[ct])
    {
        int pos;
        if((pos=LoadPersona(argv[ct],&u))==-1)
            printf("%s: Not found.\n",argv[ct]);
        else
        {
            u.uff_Flag[9]=1;
            printf("%s: Registered.\n",argv[ct]);
            SavePersona(&u,pos);
        }

        ct++;
    }
    exit(0);
}

void Log(char *x, ...){;}

void ErrFunc(char *x, char *y, char *z, int a, char *b)
{
    fprintf(stderr,"%s %s %s %d %s\n",x,y,z,a,b);
    exit(1);
}
