APP=abermud
PREFIX?=/usr/local

CFILES  = src/NoMalloc.c src/Main.c src/NewCmd.c src/AnsiBits.c src/TableDriver.c \
	  src/TabCommand.c src/CompileTable.c src/InsAndOuts.c src/System.c src/PlyCommand.c \
	  src/ObjCommand.c src/GenCommand.c src/SysSupport.c src/RoomCommands.c src/Editing.c \
	  src/Parser.c src/SubHandler.c src/SaveLoad.c src/ComDriver.c src/BootDaemon.c \
	  src/ComServer.c src/CondCode.c src/ActionCode.c src/TimeSched.c src/UserFile.c src/Daemons.c \
	  src/TableEditing.c src/Container.c src/ExitLogic.c src/ContCommand.c src/Snoop.c \
	  src/DarkLight.c src/Duplicator.c src/FlagControl.c src/UtilCommand.c src/ObjectEdit.c \
	  src/Class.c src/LookFor.c src/UserVector.c src/FlagName.c src/FindPW.c src/runaber.c src/IPCClean.c \
	  src/IPCDirect.c src/LibSocket.c src/ValidLogin.c src/BSX.c src/sha256.c

OFILES  = src/NoMalloc.o src/Main.o src/NewCmd.o src/AnsiBits.o src/TableDriver.o \
	  src/TabCommand.o src/CompileTable.o src/InsAndOuts.o src/System.o src/PlyCommand.o \
	  src/ObjCommand.o src/GenCommand.o src/SysSupport.o src/RoomCommands.o src/Editing.o \
	  src/Parser.o src/SubHandler.o src/SaveLoad.o src/ComDriver.o src/BootDaemon.o \
	  src/ComServer.o src/CondCode.o src/ActionCode.o src/TimeSched.o src/UserFile.o src/Daemons.o \
	  src/TableEditing.o src/Container.o src/ExitLogic.o src/ContCommand.o src/Snoop.o \
	  src/DarkLight.o src/Duplicator.o src/FlagControl.o src/UtilCommand.o src/ObjectEdit.o \
	  src/Class.o src/LookFor.o src/UserVector.o src/FlagName.o \
	  src/IPCDirect.o src/LibSocket.o src/ValidLogin.o src/BSX.o src/sha256.o

HEADERS = src/System.h src/User.h src/Comms.h src/NoProto.h

LDFLAGS =
ECHO    = /bin/echo
MV	= /bin/mv
TOUCH   = touch

CC	= gcc -Wall -pedantic -g -m32 -march=native -std=gnu99 -Isrc

all     : server Run_Aber Reg
	@${ECHO}   AberMUD6 is up to date.

server  : ${OFILES}
	${CC} ${LDFLAGS} ${OFILES}
	@${TOUCH} server
	@${MV} server server.old
	@${MV} a.out server

Reg	: src/Reg.o src/UserFile.o src/AnsiBits.o
	${CC} ${LDFLAGS} src/Reg.o src/UserFile.o src/AnsiBits.o -o Reg

Reg.o	: Reg.c System.h

client  : src/Socket.o src/Client.o
	${CC} ${LDFLAGS} src/Client.o src/Socket.o -lcurses -o client

Socket.o : Socket.c

Client.o : Client.c

runaber.o : runaber.c

Run_Aber : src/Run_Aber.o
	${CC} ${LDFLAGS} src/Run_Aber.o -o Run_Aber

clean:
	rm -f src/*.o *~ server Run_Aber Reg server.old Manual.txt src/flycheck*
install:
	mkdir -p ${DESTDIR}${PREFIX}/bin
	mkdir -m 755 -p ${DESTDIR}${PREFIX}/share/man/man1
	cp man/${APP}-server.1.gz ${DESTDIR}${PREFIX}/share/man/man1
	cp server ${DESTDIR}${PREFIX}/bin/${APP}-server
uninstall:
	rm ${PREFIX}/bin/${APP}-server
	rm ${PREFIX}/share/man/man1/${APP}-*

${OFILES} : ${HEADERS}
.PRECIOUS: ${CFILES} ${HEADERS}
.DEFAULT: ${CFILES} ${HEADERS}
