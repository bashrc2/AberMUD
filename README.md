# AberMUD

<img src="https://code.freedombone.net/bashrc/AberMUD/raw/master/logo.png?raw=true" width=400/>

Originally developed by Alan Cox and first released written in C in 1989.

## Installation

On An Arch/Parabola based system:

``` bash
sudo pacman -S gcc multilib-devel
```

Or on a Debian based system:

``` bash
sudo apt-get install gcc-8-multilib
```

Then build:

``` bash
make server
sudo make install
```

## Usage

Run the server:

``` bash
cp abernew/motd .
touch UAF
abermud-server -p 5000 blizzard32.uni
```

Login from another system:

``` bash
telnet [domain or IP] 5000
```

# Running as a daemon

To create a systemd daemon:

``` bash
git clone https://code.freedombone.net/bashrc/abermud /etc/abermud
cd /etc/abermud
cp abernew/motd .
touch UAF
cp blizzard32.uni mud.uni
useradd -d /etc/abermud/ abermud
cp abermud.service /etc/systemd/system/abermud.service
systemctl enable abermud.service
systemctl daemon-reload
systemctl start abermud.service
```

# Editing and saving the database

You will first need to have a file called *.allowarchwizard* in the same directory as the server.

``` bash
touch .allowarchwizard
```

For help on making changes to the universe see the manpage:


``` bash
man abermud-server
```

Login with username *Anarchy*, then run the command:

``` bash
saveuniverse [filename]
```

Typically with a *.uni* file extension.

When you are finished editing stop the server and remove the file:

``` bash
rm .allowarchwizard
```
